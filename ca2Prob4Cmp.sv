module cmpP4(input [1:0]a,[1:0]b, output GT,EQ);
  logic [7:0]j;
  logic [7:0]k;
  logic [2:0]s1;
  //S
  assign s1[0]=a[0];assign s1[1]=a[1];assign s1[2]=b[1];
  //GT
  assign j[0]=0;assign j[4]=0;assign j[5]=0;assign j[6]=0;
  assign j[2]=1;assign j[3]=1;
  assign j[1]=~b[0];assign j[7]=~b[0];
  //EQ
  assign k[0]=~b[0];assign k[1]=b[0];
  assign k[2]=0;assign k[3]=0;assign k[4]=0;assign k[5]=0;
  assign k[6]=~b[0];assign k[7]=b[0];
  Mux8t1P2 gt(s1[2:0],j[7:0],GT);
  Mux8t1P2 eq(s1[2:0],k[7:0],EQ);
endmodule