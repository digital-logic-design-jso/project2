module Mux8t1P2(input [2:0]s , [7:0]j, output w);
  Mux4t1P1 m1(s[1:0],j[3:0],s[2],w);
  Mux4t1P1 m2(s[1:0],j[7:4],~s[2],w);
endmodule
