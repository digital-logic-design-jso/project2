module Cmp42SP8(input [3:0]a,input [3:0]b,output GT , EQ);
  logic l;logic HEQ;logic HGT;logic LGT;logic LEQ;logic [3:0]c;logic [3:0]d;
  assign c[3]=~a[3];assign c[2]=a[2];assign c[1]=a[1];assign c[0]=a[0];
  assign d[3]=~b[3];assign d[2]=b[2];assign d[1]=b[1];assign d[0]=b[0];
  cmpP4 c1(c[1:0],d[1:0],LGT,LEQ);
  cmpP4 c2(c[3:2],d[3:2],HGT,HEQ);
  and(EQ,LEQ,HEQ);
  and(l,LGT,HEQ);
  or(GT,LGT,l);
endmodule
