module Mux4t1P1 (input [1:0]s,[3:0]j ,input en , output w);
  assign #54 w=(~en) ? ((s==0) ? j[0]:
                    (s==1) ? j[1]:
                    (s==2) ? j[2]:
                    (s==3) ? j[3]:1'bx)
                    :1'bz;
endmodule
