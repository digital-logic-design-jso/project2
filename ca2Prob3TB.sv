module MuxP2TB();
  logic [2:0]s;
  logic [7:0]j;
  logic w;
  Mux8t1P2 m1(s[2:0],j[7:0],w);
  initial begin
    j=8'b01001101;
    s=3'b000;
    #150;
    s=3'b001;
    #150;
    s=3'b011;
    #150;
    s=3'b010;
    #150;
    s=3'b110;
    #150;
    s=3'b111;
    #150;
    s=3'b101;
    #150;
    s=3'b100;
    #150; 
  $stop;  
  end
endmodule
