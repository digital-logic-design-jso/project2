module Cmp4bP6(input [3:0]a,input [3:0]b,output GT , EQ);
  logic l;logic HEQ;logic HGT;logic LGT;logic LEQ;
  cmpP4 c1(a[1:0],b[1:0],LGT,LEQ);
  cmpP4 c2(a[3:2],b[3:2],HGT,HEQ);
  and(EQ,LEQ,HEQ);
  and(l,LGT,HEQ);
  or(GT,LGT,l);
endmodule
