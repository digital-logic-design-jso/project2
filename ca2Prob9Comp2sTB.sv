module cmpP8TB();
  logic [3:0]a;
  logic [3:0]b;
  logic g;
  logic e;
  Cmp42SP8 c1(a[3:0],b[3:0],g,e);
  initial begin
    a=4'b1000;
    b=4'b1000;
    #200;
    a=4'b1001;
    b=4'b1000;
    #200;
    a=4'b1011;
    b=4'b0000;
    #200;
    a=4'b0010;
    b=4'b1000;
    #200;
    a=4'b1010;
    b=4'b0001;
    #200;
    a=4'b1011;
    b=4'b0001;
    #200;
    a=4'b0001;
    b=4'b1001;
    #200;
    a=4'b0000;
    b=4'b0001;
    #200;
    a=4'b1000;
    b=4'b1011;
    #200;
    a=4'b0001;
    b=4'b1011;
    #200;
    a=4'b1011;
    b=4'b0011;
    #200;
    a=4'b0010;
    b=4'b0011;
    #200;
    a=4'b0010;
    b=4'b0010;
    #200;
    a=4'b0011;
    b=4'b0010;
    #200;
    a=4'b0001;
    b=4'b1010;
    #200;
    a=4'b0000;
    b=4'b0010;
    #200;
    $stop;
  end
endmodule

