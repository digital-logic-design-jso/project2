module Cmp4P10a (input [3:0]a,[3:0]b,output GT,EQ);
  assign #54 GT=(a > b);
  assign #54 EQ=(a == b);
endmodule
